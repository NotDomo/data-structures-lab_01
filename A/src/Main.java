import java.util.*;


public class Main {

    static List<String> Book1 = Arrays.asList("ABA","BAB","BAB","CAB","DAB","EEF","FAA");
    static List<String> Book2 = Arrays.asList("ABA","CAB","EEF","GAS","III");
    public static void main(String[] args) {
        List<String> test = appearsOnce(Book1);
        test = difference(test, Book2);
        IO.outputToConsole(test, "Book1 - Book2");

        test = sum(Book1,Book2);
        Collections.sort(test);
        IO.outputToConsole(test, "Book1 && Book2");

        test = makeBook(Book1, Book2);
        IO.outputToConsole(test, "Formed book");
    }

    static public boolean contains(List<String> list, String element){
        for (String word: list) {
            if (word == element)
                return true;
        }
        return false;
    }

    static public List<String> appearsOnce(List<String> list) {
        List<String> output = new ArrayList<>();
        HashMap<String, Integer> frequencies = findFrequency(list);
        for (String word: list) {
            if (frequencies.get(word) == 1) {
                output.add(word);
            }
        }
        return output;
    }

    static public HashMap<String, Integer> findFrequency(List<String> list){
        HashMap<String, Integer> frequencies = new HashMap<>();
        for(String word : list){
            if(frequencies.containsKey(word)){
                frequencies.put(word, frequencies.get(word) + 1);
            } else {
                frequencies.put(word, 1);
            }
        }
        return frequencies;
    }

    static public List<String> difference(List<String> A, List<String> B){
        List<String> output = new ArrayList<>();
        for (String word: A) {
            if (!contains(B,word) && !contains(output, word))
                output.add(word);
            if (output.size() > 10)
                break;
        }
        return output;
    }

    static public List<String> sum(List<String> A, List<String> B) {
        List<String> output = new ArrayList<>();
        for (String word: A) {
            if (contains(B,word) && !contains(output, word))
                output.add(word);
            if (output.size() > 10)
                break;
        }
        return output;
    }

    static public List<String> makeBook(List<String> A, List<String> B) {
        List<String> output = new ArrayList<>();
        int count1 = 0;
        int count2 = 0;
        int i;
        while (count1 < A.size() || count2 < B.size()) {
            for (i = count1; i < A.size(); i++) {
                output.add(A.get(i));
                if (count2 < B.size() && A.get(i) == B.get(count2)) {
                    i++;
                    break;
                }
            }
            count1 = i;
            for (i = count2; i<B.size(); i++) {
                output.add(B.get(i));
                if (count1 < A.size() && B.get(i) == A.get(count1)) {
                    i++;
                    break;
                }
            }
            count2 = i;
        }
        return output;
    }

}