import java.util.Arrays;
import java.util.List;

public class IO {
    static public void outputToConsole(List<String> list, String param) {
        System.out.println(param);
        System.out.println("Size: " + list.size());
        for (String word: list) {
            System.out.print(word + " ");
        }
        System.out.println();
    }
}
