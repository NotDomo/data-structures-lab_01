import Enums.Rank;
import Enums.Suit;
import studijosKTU.ScreenKTU;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeckCards extends ScreenKTU {  // A deck of card
    static final private int cHeight = 22, cWidth = 16;
    static final private int cardsInLine = Rank.values().length;
    static final private int linesNum = Suit.values().length;
    //    static final int cardsNum = cardLine * linesNum;

    private Card selectedCard;
    private int selectedR;
    private int selectedC;
    private Card selectedCard2;
    private int selectedR2;
    private int selectedC2;

    List<Card> deck = new ArrayList();

    DeckCards() {   // konstruktorius generuoja 2 pilnas kortų kalades
        super(cHeight, cWidth, linesNum*8 + 1, cardsInLine*4 + 1, Fonts.boldB);
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                deck.add(new Card(suit, rank));
                deck.add(new Card(suit, rank));
            }
        }
    }
    public void print() {
        System.out.println("***** begin of CardDeck");
        int nr = 0;
        for (Card card : deck) {
            System.out.println("" + nr++ + card);   // print all cards
        }
        System.out.println("******* end of CardDeck");
    }
    @Override
    public void show() {
        int nr = 0;
        for (Card card : deck)
            card.show(this, nr / cardsInLine, nr++ % cardsInLine);   // show all cards
        refresh(100);
    }
    public void shuffle() {
        // use java.util.Collections' static method to shuffle the List
        Collections.shuffle(deck);
        Collections.shuffle(deck);
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        int r = e.getY() / (4 * cellH); // kortą sudaro 4x4 simboliai
        int c = e.getX() / (4 * cellW);
        Card card = deck.get(r * cardsInLine + c);
        if (selectedCard != null && selectedCard2 != null)
        {
            selectedCard.reverseFace();
            selectedCard.show(this, selectedR, selectedC);
            selectedCard2.reverseFace();
            selectedCard.show(this, selectedR2, selectedC2);
            refresh(40);
            selectedCard = null;
            selectedCard2 = null;
        }
        if (card.getSolved()) {
            System.out.println("clicked on already solved card - skipping");
        }

        else if (selectedCard == null){
            System.out.println("selected first card - marking it");
            card.reverseFace();
            selectedCard = card;
            selectedR = r;
            selectedC = c;
            card.show(this, r, c);
            refresh(40);
        }
        else if (selectedCard.getRank() == card.getRank() &&
            selectedCard.getSuit() == card.getSuit() &&
            selectedCard != card)
        {
            card.reverseFace();
            System.out.println("clicked on a match - marking them as solved");
            selectedCard.SetSolved();
            card.SetSolved();
            card.show(this, r, c);
            refresh(40);
            selectedCard = null;
        }
        else
        {
            System.out.println("clicked on second card - no match - remembering the second card");
            selectedCard2 = card;
            selectedC2 = c;
            selectedR2 = r;
            card.reverseFace();
            card.show(this, r, c);
            refresh(40);
        }

    }
}