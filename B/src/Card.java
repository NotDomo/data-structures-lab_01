import java.awt.Color;
import static java.awt.Color.*;

import Enums.Rank;
import Enums.Suit;
import studijosKTU.ScreenKTU;

class Card {  // A card
    private final Suit suit;
    private final Rank rank;
    private boolean faceUp;
    private boolean solved;
    static private final char sym10 = 0x2789;

    static private final Color[] suitColor = {black, red, black, red};
    static private final char[] suitSym = {'♠', '♦', '♣', '♥'};
    static private final char[] rankSym = {
            'A', '2', '3', '4', '5', '6', '7', '8', '9', sym10, 'J', 'Q', 'K'};

    Card(Suit suit, Rank rank) {   // constructor
        this.suit = suit;
        this.rank = rank;
        this.faceUp = false;   // jei true - tai korta atversta
        this.solved = false;
    }
    public Rank getRank() { return rank; }
    public Suit getSuit() { return suit; }
    public boolean getSolved() { return solved; }
    public void SetSolved() { solved = true; }
    public void reverseFace() {
        faceUp = !faceUp;
    }
    public String toString() {
//       return "This card is " + rank + " of " + suit;
        return ": card is " + rankSym[rank.ordinal()] + " of " + suitSym[suit.ordinal()];
    }
    public void show(ScreenKTU scr, int row, int col) {
        int r = row * 4 + 1;  // kortą sudaro 4x4 simboliai (kartu su tarpu)
        int c = col * 4 + 1;
        if (faceUp) {   // jei korta atversta
            scr.setColors(Color.white, suitColor[suit.ordinal()]);
            scr.fillRect(r, c, 3, 3);      // pagrindas bus baltas
            scr.lineBorder(r, c, 3, 3, 2); // pakraščio linija
            scr.print(r, c, rankSym[rank.ordinal()]);
            scr.print(r + 1, c + 1, suitSym[suit.ordinal()]);
            scr.print(r + 2, c + 2, rankSym[rank.ordinal()]);
        } else {           // jei korta užversta
            scr.setColors(Color.gray, Color.blue);
            scr.fillRect(r, c, 3, 3, '\u2592');
        }
    }
}