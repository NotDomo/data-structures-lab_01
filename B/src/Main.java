/** @author Eimutis Karčiauskas, KTU IF Programų inžinerijos katedra, 2013 09 09
 *
 *  Tai yra demonstracinė kortų kaladės pateikimo ekrane ScreenKTU klasė.
 *  joje taip pat pateikiamas enum taikymo pavyzdys, kuris paimtas iš
 *   http://docs.oracle.com/javase/1.5.0/docs/guide/language/enums.html
 *
 *  Atkreipkite dėmesį į metodą mouseClicked(MouseEvent e), kurio pagalba
 *  yra reaguojama į pelės paspaudimą ant pasirinktos kortos.
 *  IŠBANDYKITE įvairius kortų pateikimo variantus, pvz. visos užverstos.
 *  PAPILDYKITE taškų skaičiavimo algoritmais.
 *  SUKURKITE paprastą žaidimą.
 ****************************************************************************/

import java.awt.Color;
import static java.awt.Color.*;
import java.awt.event.MouseEvent;
import java.util.*;
import studijosKTU.ScreenKTU;

public class Main
{
    public static void main(String[] args) {
        DeckCards deck1 = new DeckCards();
        deck1.shuffle();
        deck1.shuffle();
        deck1.show();
        }

}