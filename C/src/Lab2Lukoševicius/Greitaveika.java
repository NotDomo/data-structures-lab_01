package Lab2Lukoševicius;

import java.util.*;

import studijosKTU.*;

public class Greitaveika {

    Random ag = new Random();
    static int[] tiriamiKiekiai = {20_000, 25_000, 30_000, 40_000};
    //static int[] tiriamiKiekiai = {10_000_000, 15_000_000, 20_000_000, 25_000_000};

    void uzpidlytiSarasus(LinkedList<Integer> linkedList, ArrayList<Integer> arrayList, int k) {
        for (int i = 0; i < k; i++) {
            int x = ag.nextInt();
            linkedList.add(x);
            arrayList.add(x);
        }
    }

    void tyrimasList(int n) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        uzpidlytiSarasus(linkedList, arrayList, n);
        int m = n /2;
        long t0 = System.nanoTime();
        for (int i = 0; i < n; i++) {
            arrayList.add(m, 1);
        }
        long t1 = System.nanoTime();
        for (int i = 0; i < n; i++) {
            linkedList.add(m, 1);
        }
        long t2 = System.nanoTime();
        Ks.ouf("%7d %7.4f %7.4f \n", n,
                (t1 - t0) / 1e9, (t2 - t1) / 1e9);

    }

    void sisteminisTyrimasList() {
        Timekeeper tk = new Timekeeper(tiriamiKiekiai);
        for (int n : tiriamiKiekiai) {
            LinkedList<Integer> linkedList = new LinkedList<>();
            ArrayList<Integer> arrayList = new ArrayList<>();
            uzpidlytiSarasus(linkedList, arrayList, n);
            int m = n / 2;
            tk.start();
            for (int i = 0; i < n; i++) {
                linkedList.add(m, 1);
            }
            tk.finish("LinkedList.add()");
            for (int i = 0; i < n; i++) {
                arrayList.add(m, 1);
            }
            tk.finish("ArrayList.add()");
            tk.seriesFinish();
        }
    }


    void tyrimas2(){
        long memTotal = Runtime.getRuntime().totalMemory();
        Ks.oun("memTotal= " + memTotal);
        Ks.ouf("kiekis, ArrayList, LinkedList\n");
        for (int n : tiriamiKiekiai) {
            tyrimasList(n);
        }
        sisteminisTyrimasList();
    }


    void tyrimas1() {
        long memTotal = Runtime.getRuntime().totalMemory();
        Ks.oun("memTotal= " + memTotal);
        Ks.ouf("kiekis, sqrt(x), sin(x)\n");
        for (int n : tiriamiKiekiai) {
            tyrimasMath(n);
        }
        sisteminisTyrimasMath();
    }

    void tyrimasMath(int n) {

        long t0 = System.nanoTime();
        for (int i = 0; i < n; i++) {
            int x = ag.nextInt();
            Math.sqrt(x);
        }
        long t1 = System.nanoTime();
        for (int i = 0; i < n; i++) {
            int x = ag.nextInt();
            Math.sin(x);
        }
        long t2 = System.nanoTime();
        Ks.ouf("%7d %7.4f %7.4f \n", n,
                (t1 - t0) / 1e9, (t2 - t1) / 1e9);
    }

    void sisteminisTyrimasMath() {
        Timekeeper tk = new Timekeeper(tiriamiKiekiai);

        for (int n : tiriamiKiekiai) {
            tk.start();
            for (int i = 0; i < n; i++) {
                int x = ag.nextInt();
                Math.sqrt(x);
            }
            tk.finish("sqrt(x)");
            for (int i = 0; i < n; i++) {
                int x = ag.nextInt();
                Math.sin(x);
            }
            tk.finish("sin(x)");
            tk.seriesFinish();
        }
    }

    public static void main(String[] args) {
        new Greitaveika().tyrimas2();
    }
}
