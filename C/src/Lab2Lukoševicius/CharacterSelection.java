package Lab2Lukoševicius;

import studijosKTU.ListKTUx;

public class CharacterSelection {

    public ListKTUx<Character> allChars = new ListKTUx<>(new Character());
    private static final Character example = new Character();


    public ListKTUx<Character> getDamageDealers(int damage) {
        ListKTUx<Character> temp = new ListKTUx<>(example);
        for (Character a : allChars) {
            if (a.getDamage() >= damage) {
                temp.add(a);
            }
        }
        return temp;
    }

    public ListKTUx<Character> getTanks(int health) {
        ListKTUx<Character> temp = new ListKTUx(example);
        for (Character a : allChars) {
            if (a.getHealth() >= health) {
                temp.add(a);
            }
        }
        return temp;
    }


    public ListKTUx<Character> getHealers(int health, int mana) {
        ListKTUx<Character> temp = new ListKTUx(example);
        for (Character a : allChars) {
            if (a.getHealth() <= health && a.getMana() >= mana) {
                temp.add(a);
            }
        }
        return temp;
    }

    public ListKTUx<Character> getByRole(String role) {
        ListKTUx<Character> temp = new ListKTUx(example);
        for (Character a : allChars) {
            if (a.getRole() == role)
                temp.add(a);
            }
        return temp;
    }

}
