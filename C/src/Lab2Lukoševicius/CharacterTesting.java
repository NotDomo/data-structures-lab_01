package Lab2Lukoševicius;

import studijosKTU.Ks;
import studijosKTU.ListKTUx;

import java.util.Comparator;
import java.util.Locale;

public class CharacterTesting {
    ListKTUx<Character> bandomieji = new ListKTUx<>(new Character());

    void metodoParinkimas(){
        tikrintiAtskirusAuto();
        formuotiAutoSąrašą();
        peržiūrėtiSąrašą();
        papildytiSąrašą();
        CheckParty();
        patikrintiRikiavimą();
    }

    void tikrintiAtskirusAuto() {
        Character a1 = new Character("Player1","TANK", 2000, 200,  100);
        Character a2 = new Character("Player_2","HEALER", 700, 2100, 50);
        Character a3 = new Character("Player3","TANK",3000,100,120);
        Character a4 = new Character();
        Character a5 = new Character();
        Character a6 = new Character();
        a4.parse("Player4 ROGUE 1500 1000 200");
        a5.parse("Player5 HEALER 700 2400 60");
        a6.parse("Player6 HUNTER 2100 700 180");

        Ks.oun(a1);
        Ks.oun(a2);
        Ks.oun(a3);
        Ks.oun("Average health of first 3 characters: " +
                (a1.getHealth() + a2.getHealth() + a3.getHealth())/3);
        Ks.oun(a4);
        Ks.oun(a5);
        Ks.oun(a6);
        Ks.oun("Average damage of the party:  " +
                (a1.getDamage() + a2.getDamage() + a3.getDamage() +
                        a4.getDamage() + a5.getDamage() + a6.getDamage())/6);
    }
    void formuotiAutoSąrašą() {
        Character a1 = new Character("Player1","TANK", 2000, 200,  100);
        Character a2 = new Character("Player_2","HEALER", 700, 2100, 50);
        Character a3 = new Character("Player3","TANK",3000,100,120);
        bandomieji.add(a1);
        bandomieji.add(a2);
        bandomieji.add(a3);
        bandomieji.println("First 3 characters");
        bandomieji.add("Player4 ROGUE 1500 1000 200");
        bandomieji.add("Player5 HEALER 700 2400 60");
        bandomieji.add("Player6 HUNTER 2100 700 180");

        bandomieji.println("Entire party");
        bandomieji.forEach(System.out::println);
        Ks.oun("Average health of first 3 party members " +
                (bandomieji.get(0).getHealth()+bandomieji.get(1).getHealth()+
                        bandomieji.get(2).getHealth())/3);

        Ks.oun("Sum of other 3 party members damage "+
                (bandomieji.get(3).getDamage()+bandomieji.get(4).getDamage()+
                        bandomieji.get(5).getDamage()));
         //palaipsniui atidenkite sekančias eilutes ir išbandykite
        bandomieji.add(0, new Character("NEW_PlaYer","SUMMONER",2000,4000,200));
        bandomieji.add(6, new Character("Player_100","DWARF",1998,9500,777));
        bandomieji.set(4, a3);
        bandomieji.println("Po įterpimų");
        bandomieji.remove(6);
        bandomieji.remove(0);
        bandomieji.println("Index of Player3 object: " + bandomieji.indexOf(a3));
        bandomieji.removeFirst();
        bandomieji.removeFirst();
        bandomieji.println("Po išmetimų");
        bandomieji.removeRange(1,3);
        bandomieji.println("Išmetus nuo 1 iki 3");


    }
    void peržiūrėtiSąrašą(){
        int sk=0;
        for (Character a: bandomieji){
            if (a.getRole().compareTo("TANK")==0)
                sk++;
        }
        Ks.oun("Number of tanks in the party = " +sk);
    }
    void papildytiSąrašą(){
        for (int i=0; i<8; i++){
            bandomieji.add(new Character(("PLAYER"+(i+10)), "NEW",
                    1000+i, 1000+i, 100+i));
        }
        bandomieji.add("PLAYER111 ASSASSIN 1000 0 250");
        bandomieji.add("PlayER_02 TANK 4000 500 135");
        bandomieji.add("Player_55 HEALER 1100 3400 50");
        bandomieji.println("List of test characters");
        bandomieji.save("ban.txt");
    }
    void CheckParty(){
        CharacterSelection tenp = new CharacterSelection();

        tenp.allChars.load("ban.txt");
        tenp.allChars.println("Bandomasis rinkinys");

        bandomieji = tenp.getTanks(1500);
        bandomieji.println("Characters with at least 1500 health");

        bandomieji = tenp.getDamageDealers(100);
        bandomieji.println("Characters with at least 100 damage");

        bandomieji = tenp.getByRole("TANK");
        bandomieji.println("Tank characters");

        bandomieji = tenp.getHealers(1000, 1000);
        bandomieji.println("Health =< 1000, mana >= 1000");
    }

    void patikrintiRikiavimą(){
        CharacterSelection aps = new CharacterSelection();

        aps.allChars.load("ban.txt");
        Ks.oun("========"+aps.allChars.get(0));
        aps.allChars.println("Base list");
        aps.allChars.sortBuble(Character.byDamage);
        aps.allChars.println("Sorted by damage");
        aps.allChars.sortBuble(Character.byHealth);
        aps.allChars.println("Sorted by health");
        aps.allChars.sortBuble(Character.byHealthAndMana);
        aps.allChars.println("Sorted by health and mana");
        aps.allChars.sortBuble(sortByDamage);
        aps.allChars.sortBuble((a, b) -> Integer.compare(a.getDamage(), b.getDamage()));
        aps.allChars.println("Sorted by damage - again");
        aps.allChars.sortBuble();
        aps.allChars.println("Sorted with compareTo - damage, health, mana");
    }

    static Comparator sortByDamage = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            int r1 = ((Character) o1).getDamage();
            int r2 = ((Character) o2).getDamage();
            if (r1 < r2) return 1;
            if (r1 > r2) return -1;
            return 0;
        }
    };
    public static void main(String... args) {
        new CharacterTesting().metodoParinkimas();
    }
}
