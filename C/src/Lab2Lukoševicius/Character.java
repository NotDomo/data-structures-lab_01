package Lab2Lukoševicius;

import studijosKTU.KTUable;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import studijosKTU.*;

public class Character implements KTUable<Character> {

    final static private int minDamage = 10;
    final static private int maxDamage = 1000;
    final static private int minHealth = 100;
    final static private int minMana = 50;

    private String name;
    private String role;
    private int health;
    private int mana;
    private int damage;

    public Character() {}

    public Character(String name, String role, int health, int mana, int damage) {
        this.name = name;
        this.role = role;
        this.health = health;
        this.mana = mana;
        this.damage = damage;
    }

    public Character(String dataString){
        this.parse(dataString);
    }

    @Override
    public Character create(String dataString) {
        Character a = new Character();
        a.parse(dataString);
        return a;
    }
    @Override
    public final void parse(String dataString) {
        try {
            Scanner ed = new Scanner(dataString);
            name   = ed.next();
            role   = ed.next();
            health = ed.nextInt();
            mana   = ed.nextInt();
            damage = ed.nextInt();
        } catch (InputMismatchException  e) {
            Ks.ern("Bad character data format -> " + dataString);
        } catch (NoSuchElementException e) {
            Ks.ern("Some data is mising -> " + dataString);
        }
    }
    @Override
    public String validate() {
        String ex = "";
        if (mana < minMana)
            ex = "Character's mana must be higher than " + minMana;
        if (health < minHealth)
            ex += " Character's health must be higher than " + minHealth;
        if (damage < minDamage || damage > maxDamage)
            ex += " Character's damage must be between " + minDamage +
                    " and " + maxDamage;
        return ex;
    }
    @Override
    public String toString(){
        return String.format("%-16s %-12s %5d %5d %5d",
                name, role, health, mana, damage, validate());
    };
    public String getName() { return name; }
    public String getRole() { return role; }
    public int getHealth() { return health; }
    public int getMana() { return mana; }
    public int getDamage() { return damage; }

    @Override
    public int compareTo(Character a) {
        // compares characters by damage
        if (damage < a.getDamage()) return -1;
        if (damage > a.getDamage()) return +1;
        if (health < a.getHealth()) return -1;
        if (health > a.getHealth()) return +1;
        if (mana   < a.getMana()) return -1;
        if (mana   > a.getMana()) return +1;
        return 0;
    }

    public final static Comparator byDamage = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Character a1 = (Character) o1;
            Character a2 = (Character) o2;
            if(a1.getDamage() < a2.getDamage()) return 1;
            if(a1.getDamage() > a2.getDamage()) return -1;
            return 0;
        }
    };

    public final static Comparator byHealth = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Character a1 = (Character) o1;
            Character a2 = (Character) o2;
            if(a1.getHealth() < a2.getHealth()) return 1;
            if(a1.getHealth() > a2.getHealth()) return -1;
            return 0;
        }
    };

    public final static Comparator byHealthAndMana = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Character a1 = (Character) o1;
            Character a2 = (Character) o2;
            if(a1.getHealth() < a2.getHealth()) return 1;
            if(a1.getHealth() > a2.getHealth()) return -1;
            if(a1.getMana() < a2.getMana()) return 1;
            if(a1.getMana() > a2.getMana()) return -1;
            return 0;
        }
    };

    public static void main(String[] args){
        Character a1 = new Character("Player1","TANK", 2000, 200,  100);
        Character a2 = new Character("Player_2","HEALER", 700, 2100, 50);
        Character a3 = new Character();
        Character a4 = new Character();
        a3.parse("Player.3 ROGUE 1400 600 210");
        a4.parse("PLay3r4 WIZARD 1000 2500 180");
        Ks.oun(a1);
        Ks.oun(a2);
        Ks.oun(a3);
        Ks.oun(a4);
    }


}
